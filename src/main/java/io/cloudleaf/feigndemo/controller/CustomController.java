package io.cloudleaf.feigndemo.controller;

import io.cloudleaf.feigndemo.feign.IFeign;
import io.cloudleaf.feigndemo.model.PostRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api")
public class CustomController {
    @Autowired
    IFeign iFeign;

    @GetMapping("v1/todos")
        //@HystrixCommand(fallbackMethod = "defaultFaultBackMethod")
    ResponseEntity<?> getTodoList() {
        return ResponseEntity.ok(iFeign.getTodos());
    }


    @RequestMapping(value = "v1/posts", method = RequestMethod.POST)
    ResponseEntity<?> submitPosts(@RequestBody PostRequest post) {
        return ResponseEntity.ok(iFeign.submitPost(post));
    }



}
