package io.cloudleaf.feigndemo.servletfilter;


import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
public class SimpleFilter implements Filter {
    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterchain)
            throws IOException, ServletException {
        HttpServletRequest hp = (HttpServletRequest) request;
        // System.out.println("Remote Host:"+request.getRemoteHost());
        //  System.out.println("Remote Address:"+request.getRemoteAddr());
        try {
            if ("POST".equalsIgnoreCase(hp.getMethod())) {
                String test = hp.getInputStream().readAllBytes().toString();
                System.out.println(test);
            }
        } catch (Exception ex) {

        }

        filterchain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig filterconfig) throws ServletException {
    }
}