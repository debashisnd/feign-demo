package io.cloudleaf.feigndemo.servletfilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
@ControllerAdvice
public class GlobalControllerAdvice {
    private final Logger log = LoggerFactory.getLogger(getClass());

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleException(Exception exception, HttpServletRequest req) {
        log.info("---------------->Error "  + exception.toString() + req.getMethod() + "->" + req.getRequestURI());
        return ResponseEntity.ok("Thanks ");
    }


}
