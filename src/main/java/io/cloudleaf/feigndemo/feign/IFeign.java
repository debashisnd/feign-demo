package io.cloudleaf.feigndemo.feign;

import io.cloudleaf.feigndemo.model.PostRequest;
import io.cloudleaf.feigndemo.model.PostResponse;
import io.cloudleaf.feigndemo.model.TodoRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@FeignClient(name = "${feign.name}", url = "${feign.url}", fallbackFactory = CustomException.class)
public interface IFeign {

    @GetMapping("/todos")
    List<TodoRequest> getTodos();

    @PostMapping("/posts")
    PostResponse submitPost(PostRequest request);
}
