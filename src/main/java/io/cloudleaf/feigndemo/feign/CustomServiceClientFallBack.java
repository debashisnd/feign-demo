package io.cloudleaf.feigndemo.feign;

import io.cloudleaf.feigndemo.model.PostRequest;
import io.cloudleaf.feigndemo.model.PostResponse;
import io.cloudleaf.feigndemo.model.TodoRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;


public class CustomServiceClientFallBack implements IFeign {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    private final Throwable cause;
    public CustomServiceClientFallBack(Throwable cause) {
        this.cause = cause;
    }

    @Override
    public List<TodoRequest> getTodos() {
        logger.info("Debashis "  + cause.getLocalizedMessage());
        return new ArrayList<>();
    }

    @Override
    public PostResponse submitPost(PostRequest request) {
        logger.info("Debashis "  + cause.getLocalizedMessage());
        PostResponse res = new PostResponse();
        return res;
    }
}
