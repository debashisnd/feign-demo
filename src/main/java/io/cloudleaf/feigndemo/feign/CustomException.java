package io.cloudleaf.feigndemo.feign;

import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class CustomException implements FallbackFactory<IFeign> {
    @Override
    public IFeign create(Throwable cause) {
        return new CustomServiceClientFallBack(cause);
    }
}
